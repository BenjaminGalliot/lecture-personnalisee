# Fonctionnalités

Idéalement, à titre personnel, un livre électronique devrait pouvoir avoir les fonctionnalités suivantes, si possible en changement à la volée :
- modification du style global : police, ligatures, taille, etc.
- positionnement des notes (avec occultation des numéros de renvoi possible) : infrapaginales, infrachapitrales, infradocumentaires, en bulle, etc. 
- changement des graphies : français rectifié ou non, chinois traditionnel ou simplifié, transcription pinyin ou de l’École française d’Extrême-Orient, etc.
- marquage des noms propres (anthroponymes, toponymes, etc.), des emprunts, des renvois, des différences de version, etc.
- mise en forme des dialogues : à la française (« — »), à l’américaine (—) uniquement, etc.
- choix de la typographie fine : espaces fines pré-ponctuation double, guillemets imbriqués, apostrophe typographique, etc.

# Remarques diverses

## Des notes

Certaines personnes ne semblent pas aimer les notes infrapaginales, surtout quand elles deviennent trop longues, notamment relativement au corps du texte. Cela ne me semble pas justifié, pour éviter cela, de systématiquement mettre uniquement des notes infrachapitrales, comme je le vois souvent sur les recueils de contes chinois.
En effet, cela oblige le lecteur du papier à avoir toujours un pouce placé à la fin du chapitre pour rapidement alterner entre le corps de texte et les notes (s’il ne désire pas plusieurs fois par page chercher le début desdites notes…), ce qui est pour le moins gênant, sans oublier les risque de divulgâchage puisque ces notes suivent souvent directement la fin du chapitre…
De plus, en quoi est-ce si gênant d’avoir les toutes premières ou dernières pages bien remplies de notes, au vu de tous les avantages procurés ? Il est vrai que l’on tourne plus vite ces pages, mais en quoi est-ce différent d’avoir à tourner directement une page de garde de chapitre (avec uniquement son nom) régulièrement, ou une page de dialogue qui se lit très vite et parfois très à la verticale ? S’il s’agit de la quantité de texte sur lesdites page, cela ne semble pas non plus très différent de l’écart qu’il peut y avoir entre un livre de poche et sa version grand format plus aéré…

## De la mise en forme des dialogues

À titre personnel, je trouve les dialogues à l’américaine très dangeureux au niveau de la compréhension.
Effectivement, dans les dialogues à la française, il y a ouverture des guillemets au début, fermeture à la fin, et changement d’interlocuteur au milieu le cas échéant (avec le tiret cadratin).Dans la version américaines, ces symboles sont tous fusionnés en seul tiret cadratin, qui sert à la fois d’ouverture et de changement d’interlocuteur. Beaucoup d’éditeurs semblent apprécier à l’heure actuelle cette supposée clarté, qui demande pourtant beaucoup d’efforts supplémentaires pour éviter une mélecture du dialogue : en effet, que se passe-t-il lorsque s’ajoute à la fin d’un paragraphe une incise du narrateur qui n’est pas précédée d’un retour à la ligne, suivie d’un nouveau tiret cadratin lié au même interlocuteur que précédemment…?
Je ne parle pas non plus des dialogues courts situés en milieu de ligne, plus délicats à clarifier… Finalement, le gain apporté par la suppression des guillemets impose l’ajout d’un autre symbole, invisible : le saut de ligne, qui devrait systématiquement être placé avant chaque nouveau départ de dialogue (et donc sans tiret cadratin devant), pour éviter de faire croire à un changement d’interlocuteur à cause de la présence bien visuelle de plusieurs tirets…



